FROM registry.vecps.com/library/golang:alpine

LABEL author=xiaolong@caicloud.io

COPY welcome.go .
RUN go build welcome.go

EXPOSE 8000

ENTRYPOINT ["./welcome"]
